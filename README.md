# Environnement de déploiement pour le framework FriendOfPastryGarden

L'objectif du document est de donner les moyens de déployer un environnement permettant d'utiliser
l'application. Le déploiement s'appuie sur la technologie *Docker*

## Prérequis

- Docker et l'extension Docker-compose doivent être installé sur la machine

## Contexte technique

La stack technique retenue pour le framework est le suivant :
- PHP 8.2

## Installation

1. Positionner vous sur le répertoire local et récupérer le dépôt GIT via la commande suivante

```
git clone https://gitlab.adullact.net/friend-of-pastry-garden/docker.git fopg
```

2. Configuration du proxy

Si l'environnement local passe au travers d'un proxy, il faut renseigner et décommenter dans php8/Dockerfile l'information proxy

### Affichage du code à mettre à jour
```
# A n'activer que dans un environnement local sous proxy
#ENV http_proxy "http://rie-proxy.justice.gouv.fr:8080"
#ENV https_proxy "http://rie-proxy.justice.gouv.fr:8080"
#ENV ftp_proxy "http://rie-proxy.justice.gouv.fr:8080"
#ENV no_proxy ".intranet.justice.gouv.fr,localhost,127.0.0.1,selenium,selenium_firefox"
```

3. Lancer la construction des services Docker

```
cd fopg
docker-compose up -d
```

4. Sur le répertoire principal, exécuter les commandes suivantes

```
mkdir app/symfony/component &&
sudo chown -R $USER:$USER app/ &&
cd app/symfony/component &&
git clone https://gitlab.adullact.net/friend-of-pastry-garden/component/utilsbundle.git UtilsBundle &&
git clone https://gitlab.adullact.net/friend-of-pastry-garden/component/utilsbundle.git RpaBundle
```

5. Mise à jour de l'environnement de développement

Exécuter la commande de connection à l'environnement de développement

```
docker exec -ti fopg-php8-instance /bin/bash
```

Une fois connecté, exécuter les commandes suivantes :
̀```
cd /var/www/html/component/UtilsBundle
composer install
cd /var/www/html/component/RpaBundle
composer install
̀```
